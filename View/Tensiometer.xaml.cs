﻿using DatabaseApp.Model;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for Tensiometer.xaml
    /// </summary>
    public partial class Tensiometer : UserControl
    {
        private TensiometerViewModel tvm;
        private bool isUpdating = false;
        private Int64 tensometerToUpdateID = 0;
        public Tensiometer()
        {
            InitializeComponent();
            tvm = new TensiometerViewModel();

            tvm.GetAllTensiometers();
            TensoGrid.ItemsSource = tvm.GetTensiometers;
            TensoGrid.Items.Refresh();

            tvm.GetAllMachines();
            VINComboBox.ItemsSource = tvm.GetMachines;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            TensiometerSnackBar.IsActive = false;

        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;
            
            if (VINComboBox.SelectedValue != null)
            {
                string result = string.Empty;

                var VIN = VINComboBox.SelectedValue.ToString();
                var isActive = Convert.ToBoolean(isActiveComboBox.SelectedIndex);
                var note = noteTextBox.Text;

                if (!isUpdating)
                {
                    result = tvm.InsertTensometer(VIN, isActive, note);

                } else
                {
                    result = tvm.UpdateTensometer(tensometerToUpdateID, VIN, isActive, note);
                }

                tvm.GetAllTensiometers();
                TensoGrid.ItemsSource = tvm.GetTensiometers;
                TensoGrid.Items.Refresh();

                TensiometerSnackBar.Message.Content = result;
                TensiometerSnackBar.IsActive = true;
            }
            else
            {
                TensiometerSnackBar.Message.Content = "Špatně vybrané VIN.";
                TensiometerSnackBar.IsActive = true;
            }

            
            isUpdating = false;
            VINComboBox.SelectedItem = null;
            isActiveComboBox.SelectedIndex = 0;
            noteTextBox.Text = "";
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            TensiometerModel tensoToDelete = ((FrameworkElement)sender).DataContext as TensiometerModel;
            string result = tvm.DeleteTensometer(tensoToDelete.TensiometerID);

            TensiometerSnackBar.Message.Content = result;
            TensiometerSnackBar.IsActive = true;

            tvm.GetAllTensiometers();
            TensoGrid.ItemsSource = tvm.GetTensiometers;
            TensoGrid.Items.Refresh();
        }

        private void Update_Button_Click(object sender, RoutedEventArgs e)
        {
            TensiometerModel tensoToUpdate = ((FrameworkElement)sender).DataContext as TensiometerModel;
            tensometerToUpdateID = tensoToUpdate.TensiometerID;

            VINComboBox.SelectedValue = tensoToUpdate.MachineVIN;
            isActiveComboBox.SelectedIndex = tensoToUpdate.IsActive ? 1 : 0;
            noteTextBox.Text = tensoToUpdate.Note;

            isUpdating = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            VINComboBox.SelectedItem = null;
            isActiveComboBox.SelectedIndex = 0;
            noteTextBox.Text = "";

            isUpdating = false;
        }

        private void Search_ID_Button(object sender, RoutedEventArgs e)
        {
            SearchByID();
        }

        private void Search_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchByID();
            }
        }

        private void SearchByID()
        {
            if (searchTenzoIDTextBox.Text.Length > 0)
            {
                tvm.SearchTensometerByID(Convert.ToInt64(searchTenzoIDTextBox.Text));
                TensoGrid.ItemsSource = tvm.GetTensiometers;
                TensoGrid.Items.Refresh();
            }
            else
            {
                tvm.GetAllTensiometers();
                TensoGrid.ItemsSource = tvm.GetTensiometers;
                TensoGrid.Items.Refresh();
            }
        }

        private void Search_VIN_Button(object sender, RoutedEventArgs e)
        {
            SearchByVIN();
        }

        private void Search_VIN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchByVIN();
            }
        }

        private void SearchByVIN()
        {
            if (searchVINTextBox.Text.Length > 0)
            {
                tvm.SearchTensometerByVIN(searchVINTextBox.Text);
                TensoGrid.ItemsSource = tvm.GetTensiometers;
                TensoGrid.Items.Refresh();
            }
            else
            {
                tvm.GetAllTensiometers();
                TensoGrid.ItemsSource = tvm.GetTensiometers;
                TensoGrid.Items.Refresh();
            }
        }

        private void Search_By_Activity(object sender, RoutedEventArgs e)
        {
            tvm.SearchTensometerByActiveState(Convert.ToBoolean(searchIsActiveComboBox.SelectedIndex));
            TensoGrid.ItemsSource = tvm.GetTensiometers;
            TensoGrid.Items.Refresh();
        }

        private void Cancel_Search_By_Activity(object sender, RoutedEventArgs e)
        {
            tvm.GetAllTensiometers();
            TensoGrid.ItemsSource = tvm.GetTensiometers;
            TensoGrid.Items.Refresh();
        }
    }
}
