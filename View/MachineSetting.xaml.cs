﻿using DatabaseApp.Model;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for MachineSetting.xaml
    /// </summary>
    public partial class MachineSetting : UserControl
    {
        private MachineSettingViewModel msvm;
        private bool isUpdating = false;
        private Int64 settingToUpdate = 0;

        public MachineSetting()
        {
            InitializeComponent();
            msvm = new MachineSettingViewModel();
            msvm.GetAllMachineSettings();
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }

        // only numbers input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            MachineSettingSnackBar.IsActive = false;
        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            isUpdating = false;
            sendStateComboBox.SelectedIndex = 0;
            sendGPSComboBox.SelectedIndex = 0;
            sendAxceStateComboBox.SelectedIndex = 0;
            sendTensoStateComboBox.SelectedIndex = 0;
        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

            var sendState = Convert.ToBoolean(sendStateComboBox.SelectedIndex);
            var sendGps = Convert.ToBoolean(sendGPSComboBox.SelectedIndex);
            var sendAxcel = Convert.ToBoolean(sendAxceStateComboBox.SelectedIndex);
            var sendTenso = Convert.ToBoolean(sendTensoStateComboBox.SelectedIndex);

            string result = string.Empty;
            if (!isUpdating)
            {
                result = msvm.AddMachineSetting(sendState, sendGps, sendAxcel, sendTenso);
            } else
            {
                result = msvm.UpdateMachineSetting(settingToUpdate, sendState, sendGps, sendAxcel, sendTenso);
            }
            

            msvm.GetAllMachineSettings();
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();

            MachineSettingSnackBar.Message.Content = result;
            MachineSettingSnackBar.IsActive = true;


            isUpdating = false;
            sendStateComboBox.SelectedIndex = 0;
            sendGPSComboBox.SelectedIndex = 0;
            sendAxceStateComboBox.SelectedIndex = 0;
            sendTensoStateComboBox.SelectedIndex = 0;
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            MachineSettingModel machineToDelete = ((FrameworkElement)sender).DataContext as MachineSettingModel;
            string result = msvm.DeleteMachneSetting(machineToDelete.MachineSettingID);

            msvm.GetAllMachineSettings();
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();

            MachineSettingSnackBar.Message.Content = result;
            MachineSettingSnackBar.IsActive = true;
        }

        private void Update_Button_Click(object sender, RoutedEventArgs e)
        {
            MachineSettingModel machineToUpdate = ((FrameworkElement)sender).DataContext as MachineSettingModel;

            settingToUpdate = machineToUpdate.MachineSettingID;
            sendStateComboBox.SelectedIndex = machineToUpdate.SendState ? 1 : 0;
            sendGPSComboBox.SelectedIndex = machineToUpdate.SendGPS ? 1 : 0;
            sendAxceStateComboBox.SelectedIndex = machineToUpdate.SendAxcelerometer ? 1 : 0;
            sendTensoStateComboBox.SelectedIndex = machineToUpdate.SendTensiometer ? 1 : 0;

            isUpdating = true;
        }

        private void Search_By_ID(object sender, RoutedEventArgs e)
        {
            SearchByID();
        }

        private void SearchByID()
        {
            if (searchSettingIDTextBox.Text.Length > 0)
            {
                msvm.SearchByID(Convert.ToInt64(searchSettingIDTextBox.Text));
                MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
                MachineSettingGrid.Items.Refresh();
            }
            else
            {
                msvm.GetAllMachineSettings();
                MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
                MachineSettingGrid.Items.Refresh();
            }
        }

        private void Search_By_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchByID();
            }
        }

        private void Search_Send_State(object sender, RoutedEventArgs e)
        {
            msvm.SearchBySendState(Convert.ToBoolean(searchSendStateComboBox.SelectedIndex));
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }

        private void Cancel_Search(object sender, RoutedEventArgs e)
        {
            msvm.GetAllMachineSettings();
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }

        private void Search_Send_GPS(object sender, RoutedEventArgs e)
        {
            msvm.SearchByGPSState(Convert.ToBoolean(searchSendGPSComboBox.SelectedIndex));
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }

        private void Search_Send_Axcel(object sender, RoutedEventArgs e)
        {
            msvm.SearchByAxcelerometerState(Convert.ToBoolean(searchSendAxcelComboBox.SelectedIndex));
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }

        private void Search_Send_Tenso(object sender, RoutedEventArgs e)
        {
            msvm.SearchByTensometer(Convert.ToBoolean(searchSendTensiComboBox.SelectedIndex));
            MachineSettingGrid.ItemsSource = msvm.GetMachinesSetting;
            MachineSettingGrid.Items.Refresh();
        }
    }
}
