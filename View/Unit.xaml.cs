﻿using DatabaseApp.Model;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for Unit.xaml
    /// </summary>
    public partial class Unit : UserControl
    {
        private UnitViewModel uvm;
        private bool isUpdating = false;
        private Int64 unitUpdateID = 0;

        public Unit()
        {
            InitializeComponent();
            uvm = new UnitViewModel();

            uvm.GetAllUnits();
            UnitGrid.ItemsSource = uvm.GetUnits;
            UnitGrid.Items.Refresh();

            uvm.GetAllUnitSettings();
            settingID.ItemsSource = uvm.GetUnitSettings;
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            SearchUnit();
        }

        private void SearchVin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchUnit();
            }
        }
        
        private void SearchUnit()
        {
            if (searchUnitTextBox.Text.Length > 0)
            {
                uvm.SearchUnits(Convert.ToInt64(searchUnitTextBox.Text));
                UnitGrid.ItemsSource = uvm.GetUnits;
                UnitGrid.Items.Refresh();
            }
            else
            {
                uvm.GetAllUnits();
                UnitGrid.ItemsSource = uvm.GetUnits;
                UnitGrid.Items.Refresh();
            }
        }
        
        // only numbers input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

            if (!string.IsNullOrEmpty(settingID.SelectedValue.ToString()))
            {
                string result = string.Empty;
                
                if(!isUpdating)
                {
                    result = uvm.AddUnit(Convert.ToInt32(settingID.SelectedValue));
                }
                else
                {
                    result = uvm.UpdateUnit(unitUpdateID, Convert.ToInt32(settingID.SelectedValue));
                }

                uvm.GetAllUnits();
                UnitGrid.ItemsSource = uvm.GetUnits;
                UnitGrid.Items.Refresh();

                UnitSnackBar.Message.Content = result;
                UnitSnackBar.IsActive = true;
            }
            else
            {
                UnitSnackBar.Message.Content = "Špatné parametry pro přidání jednotky.";
                UnitSnackBar.IsActive = true;
            }

            isUpdating = false;
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            UnitSnackBar.IsActive = false;
        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            isUpdating = false;
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            UnitModel machineToDelete = ((FrameworkElement)sender).DataContext as UnitModel;
            string result = uvm.DeleteUnit(machineToDelete.UnitSerialNumber);
            UnitSnackBar.Message.Content = result;
            UnitSnackBar.IsActive = true;

            uvm.GetAllUnits();
            UnitGrid.ItemsSource = uvm.GetUnits;
            UnitGrid.Items.Refresh();
        }

        private void Update_Button_Click(object sender, RoutedEventArgs e)
        {
            UnitModel machineToUpdate = ((FrameworkElement)sender).DataContext as UnitModel;
            unitUpdateID = machineToUpdate.UnitSerialNumber;
            isUpdating = true;
        }
    }
}
