﻿using DatabaseApp.Model;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for UnitSetting.xaml
    /// </summary>
    public partial class UnitSetting : UserControl
    {
        private UnitSettingViewModel usvm;
        private bool isUpdating = false;
        private Int64 unitSettingId = 0;

        public UnitSetting()
        {
            InitializeComponent();

            usvm = new UnitSettingViewModel();
            usvm.GetAllUnitSettings();
            UnitGrid.ItemsSource = usvm.GetUnitSettings;
            UnitGrid.Items.Refresh();
        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;
            
            if (!string.IsNullOrEmpty(intervalTextBox.Text))
            {
                string result = string.Empty;
                var interval = int.Parse(intervalTextBox.Text);
                var sendBatteryState = Convert.ToBoolean(sendBatteryStateComboBox.SelectedIndex);

                // result = usvm.InserUnitSetting(interval, sendBatteryState);
                
                if (!isUpdating)
                {
                    result = usvm.InserUnitSetting(interval, sendBatteryState);
                }
                else
                {
                    result = usvm.UpdateUnitSetting(unitSettingId, interval, sendBatteryState);
                    // result = uvm.UpdateUnit(unitUpdateID, Convert.ToInt32(settingID.SelectedValue));
                }
                
                usvm.GetAllUnitSettings();
                UnitGrid.ItemsSource = usvm.GetUnitSettings;
                UnitGrid.Items.Refresh();

                UnitSettingSnackBar.Message.Content = result;
                UnitSettingSnackBar.IsActive = true;
            }
            else
            {
                UnitSettingSnackBar.Message.Content = "Špatné parametry pro přidání nastavení jednotky.";
                UnitSettingSnackBar.IsActive = true;
            }

            isUpdating = false;
            intervalTextBox.Text = string.Empty;
            sendBatteryStateComboBox.SelectedIndex = 0;

        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            isUpdating = false;
            intervalTextBox.Text = string.Empty;
            sendBatteryStateComboBox.SelectedIndex = 0;
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            UnitSettingSnackBar.IsActive = false;
            
        }

        // only numbers input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            UnitSettingModel unitSettingToDelete = ((FrameworkElement)sender).DataContext as UnitSettingModel;
            string result = usvm.DeleteUnitSetting(unitSettingToDelete.UnitSettingID);
            UnitSettingSnackBar.Message.Content = result;
            UnitSettingSnackBar.IsActive = true;

            usvm.GetAllUnitSettings();
            UnitGrid.ItemsSource = usvm.GetUnitSettings;
            UnitGrid.Items.Refresh();
        }

        private void Update_Button_Click(object sender, RoutedEventArgs e)
        {
            UnitSettingModel unitSetting = ((FrameworkElement)sender).DataContext as UnitSettingModel;
            unitSettingId = unitSetting.UnitSettingID;
            intervalTextBox.Text = unitSetting.Interval.ToString();
            sendBatteryStateComboBox.SelectedIndex = unitSetting.SendBatteryState ? 1 : 0;
            isUpdating = true;
        }

        private void Search_SettingID_Button(object sender, RoutedEventArgs e)
        {
            Search(false);
        }

        private void Search_SettingID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Search(false);
            }
        }

        private void Search_By_State(object sender, RoutedEventArgs e)
        {
            Search(true);
        }

        private void Search(bool searchByBatteryState)
        {
            if (searchSettingIDTextBox.Text.Length > 0)
            {
                Int64 ID = Convert.ToInt64(searchSettingIDTextBox.Text);
                usvm.SearchUnitSettingByID(ID);
            }
            else if (searchIntervalTextBox.Text.Length > 0)
            {
                int interval = Convert.ToInt32(searchIntervalTextBox.Text);
                usvm.SearchUnitSettingByInterval(interval);
            }
            else if (searchByBatteryState)
            {
                bool sendState = Convert.ToBoolean(searchBatteryStateComboBox.SelectedIndex);
                usvm.SearchUnitSettingByState(sendState);
            }
            else
            {
                usvm.GetAllUnitSettings();
            }

            UnitGrid.ItemsSource = usvm.GetUnitSettings;
            UnitGrid.Items.Refresh();
        }

        private void Cnacel_Search_Button(object sender, RoutedEventArgs e)
        {
            Search(false);
        }
    }
}
