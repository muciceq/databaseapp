﻿using DatabaseApp.Model;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for Machine.xaml
    /// </summary>
    public partial class Machine : UserControl
    {
        private MachineViewModel mvm;
        private bool isUpdating = false;
        private string machineVinUpdate = string.Empty;

        public Machine()
        {
            
            InitializeComponent();
            mvm = new MachineViewModel();
            mvm.GetAllMachines();
            
            MachineGrid.ItemsSource = mvm.GetMachines;
            MachineGrid.Items.Refresh();

            mvm.GetUnusedUnits();
            UnitSNComboBox.ItemsSource = mvm.GetUnits;

            mvm.GetAllMachineSettings();
            MachineSettingIDComboBox.ItemsSource = mvm.GetMachineSettings;
        }

        private void Button_Click_Search(object sender, RoutedEventArgs e)
        {
            SearchVINMethod();
        }

        private void SearchVin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                SearchVINMethod();
            }
        }

        private void SearchVINMethod()
        {
            if (SearchVin.Text.Length > 0)
            {
                mvm.SearchMachines(SearchVin.Text);
                MachineGrid.ItemsSource = mvm.GetMachines;
                MachineGrid.Items.Refresh();
            }
            else
            {
                mvm.GetAllMachines();
                MachineGrid.ItemsSource = mvm.GetMachines;
                MachineGrid.Items.Refresh();
            }
        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

            if (VinTextBox.Text.Length != 17)
            {
                BadInputMachineSnackBar.Message.Content = "Špatné VIN (musí obsahovat 17 znaků)";
                BadInputMachineSnackBar.IsActive = true;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(VinTextBox.Text)  && 
                    !string.IsNullOrEmpty(UnitSNComboBox.SelectedValue.ToString()) &&
                    !string.IsNullOrEmpty(MachineSettingIDComboBox.SelectedValue.ToString()))
                {
                    // prasecina
                    string result = string.Empty;
                    if (!isUpdating)
                    {
                        result = mvm.AddMachine(VinTextBox.Text, Convert.ToInt32(UnitSNComboBox.SelectedValue),
                        Convert.ToInt32(MachineSettingIDComboBox.SelectedValue));
                    }
                    else
                    {
                        result = mvm.UpdateMachine(VinTextBox.Text, Convert.ToInt32(UnitSNComboBox.SelectedValue),
                        Convert.ToInt32(MachineSettingIDComboBox.SelectedValue), machineVinUpdate);
                    }

                    mvm.GetAllMachines();
                    MachineGrid.ItemsSource = mvm.GetMachines;
                    MachineGrid.Items.Refresh();

                    BadInputMachineSnackBar.Message.Content = result;
                    BadInputMachineSnackBar.IsActive = true;

                    mvm.GetUnusedUnits();
                    UnitSNComboBox.ItemsSource = mvm.GetUnits;

                    

                } 
                else
                {
                    BadInputMachineSnackBar.Message.Content = "Špatné parametry pro přidání stroje.";
                    BadInputMachineSnackBar.IsActive = true;
                }
            }

            
            VinTextBox.Text = string.Empty;
            isUpdating = false;
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            BadInputMachineSnackBar.IsActive = false;
        }

        private void Delete_Button(object sender, RoutedEventArgs e)
        {
            MachineModel machineToDelete = ((FrameworkElement)sender).DataContext as MachineModel;
            string message = mvm.DeleteMachine(machineToDelete.VIN);

            BadInputMachineSnackBar.Message.Content = message;
            BadInputMachineSnackBar.IsActive = true;

            mvm.GetUnusedUnits();
            UnitSNComboBox.ItemsSource = mvm.GetUnits;

            mvm.SearchMachines(SearchVin.Text);
            MachineGrid.ItemsSource = mvm.GetMachines;
            MachineGrid.Items.Refresh();
        }

        private void Update_Button(object sender, RoutedEventArgs e)
        {
            MachineModel machineToUpdate = ((FrameworkElement)sender).DataContext as MachineModel;
            machineVinUpdate = machineToUpdate.VIN;
            VinTextBox.Text = machineVinUpdate;
            isUpdating = true;
        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            VinTextBox.Text = string.Empty;
            isUpdating = false;
        }
    }
}
