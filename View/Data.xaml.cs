﻿using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for Data.xaml
    /// </summary>
    public partial class Data : UserControl
    {
        private DataViewModel dvm;
        private DateTime from = DateTime.Now;
        private DateTime to = DateTime.Now;
        private bool choosingToTime = false;
        public Data()
        {
            InitializeComponent();
            dvm = new DataViewModel();

            dvm.GetAllData();
            dataGrid.ItemsSource = dvm.GetData;
            dataGrid.Items.Refresh();

            DateTime dateTime = DateTime.Now;
            long unixTimestamp = ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
            var test = 1;
        }

        private void SnackbarMessage_ActionClick(object sender, RoutedEventArgs e)
        {
            DataSnackBar.IsActive = false;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

            var combined = CombinedCalendar.SelectedDate.Value.AddSeconds(CombinedClock.Time.TimeOfDay.TotalSeconds);
            if (!choosingToTime)
            {
                from = combined;
                fromTextBlock.Text = combined.ToString();
            } else
            {
                to = combined;
                toTextBlock.Text = combined.ToString();
            }
        }

        private void Sample1_DialogHost_OnDialogOpened(object sender, DialogOpenedEventArgs eventArgs)
        {
            if (choosingToTime)
            {
                if (fromTextBlock.Text.Length > 0)
                {
                    CombinedCalendar.DisplayDateStart = from;
                }
            }
        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            choosingToTime = false;
        }

        private void Cancel_From_Button(object sender, RoutedEventArgs e)
        {
            fromTextBlock.Text = null;
        }

        private void Cancel_To_Button(object sender, RoutedEventArgs e)
        {
            toTextBlock.Text = null;
        }

        private void To_Button_Click(object sender, RoutedEventArgs e)
        {
            choosingToTime = true;
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            string searchVIN = null;
            int? machineSettingID = null;
            int? unitID = null;
            int? unitSettingID = null;
            long? startTime = null;
            long? endTime = null;
            string state = null;
            int? battery = null;
            int? axcelX = null;
            int? axcelY = null;
            int? axcelZ = null;
            int? tenso1 = null;
            int? tenso2 = null;
            int? tenso3 = null;
            int? tenso4 = null;
            int? tenso5 = null;

            if (searchVINTextBox.Text.Length > 0)
            {
                searchVIN = "%" + searchVINTextBox.Text + "%";
            }

            if (searchMachineSettingIDTextBox.Text.Length > 0)
            {
                machineSettingID = Convert.ToInt32(searchMachineSettingIDTextBox.Text);
            }

            if (searchUnitIDTextBox.Text.Length > 0)
            {
                unitID = Convert.ToInt32(searchUnitIDTextBox.Text);
            }

            if (searchUnitSettingIDTextBox.Text.Length > 0)
            {
                unitSettingID = Convert.ToInt32(searchUnitSettingIDTextBox.Text);
            }

            if (fromTextBlock.Text.Length > 0)
            {
                startTime = ((DateTimeOffset)from).ToUnixTimeSeconds();
            }

            if (toTextBlock.Text.Length > 0)
            {
                endTime = ((DateTimeOffset)to).ToUnixTimeSeconds(); ;
            }

            if (searchStateTextBox.Text.Length > 0)
            {
                state = "%" + searchStateTextBox.Text + "%";
            }

            if (batteryTextBox.Text.Length > 0)
            {
                battery = Convert.ToInt32(batteryTextBox.Text);
            }

            if (AxTextBox.Text.Length > 0)
            {
                axcelX = Convert.ToInt32(AxTextBox.Text);
            }

            if (AyTextBox.Text.Length > 0)
            {
                axcelY = Convert.ToInt32(AyTextBox.Text);
            }

            if (AzTextBox.Text.Length > 0)
            {
                axcelZ = Convert.ToInt32(AzTextBox.Text);
            }

            if (T1TextBox.Text.Length > 0)
            {
                tenso1 = Convert.ToInt32(T1TextBox.Text);
            }

            if (T2TextBox.Text.Length > 0)
            {
                tenso2 = Convert.ToInt32(T2TextBox.Text);
            }

            if (T3TextBox.Text.Length > 0)
            {
                tenso3 = Convert.ToInt32(T3TextBox.Text);
            }

            if (T4TextBox.Text.Length > 0)
            {
                tenso4 = Convert.ToInt32(T4TextBox.Text);
            }

            if (T5TextBox.Text.Length > 0)
            {
                tenso5 = Convert.ToInt32(T5TextBox.Text);
            }

            dvm.SearchData(searchVIN, machineSettingID, unitID, unitSettingID, startTime, endTime, state, battery, axcelX, axcelY, axcelZ,
                tenso1, tenso2, tenso3, tenso4, tenso5);

            dataGrid.ItemsSource = dvm.GetData;
            dataGrid.Items.Refresh();

            CancelSearchButton.Visibility = Visibility.Visible;
        }

        private void Reset_Click_Button(object sender, RoutedEventArgs e)
        {
            searchVINTextBox.Text = null;
            searchMachineSettingIDTextBox.Text = null;
            searchUnitIDTextBox.Text = null;
            searchUnitSettingIDTextBox.Text = null;
            fromTextBlock.Text = null;
            toTextBlock.Text = null;
            searchStateTextBox.Text = null;
            batteryTextBox.Text = null;
            AxTextBox.Text = null;
            AyTextBox.Text = null;
            AzTextBox.Text = null;
            T1TextBox.Text = null;
            T2TextBox.Text = null;
            T3TextBox.Text = null;
            T4TextBox.Text = null;
            T5TextBox.Text = null;

            dvm.GetAllData();
            dataGrid.ItemsSource = dvm.GetData;
            dataGrid.Items.Refresh();

            CancelSearchButton.Visibility = Visibility.Hidden;
        }
    }
}
