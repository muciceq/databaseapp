﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseApp.ViewModel;
using MaterialDesignThemes.Wpf;
using Microsoft.Maps.MapControl.WPF;

namespace DatabaseApp.View
{
    /// <summary>
    /// Interaction logic for Map.xaml
    /// </summary>
    public partial class Map : UserControl
    {
        private DataViewModel dvm;
        private DateTime from = DateTime.Now;
        private DateTime to = DateTime.Now;
        private bool choosingToTime = false;
        public Map()
        {
            InitializeComponent();
            mapTextBlock.Text = "Mapa zobrazuje: Poslední data";



            dvm = new DataViewModel();
            dvm.LoadLastData();
            var lastData = dvm.GetLastData;

            foreach (var d in lastData)
            {
                var pin = AddPushPin(d.GPSlat, d.GPSlon, "VIN: " + d.MachineVIN + "\nlatitude: {0}\nlongitude: {1}");
                myMap.Children.Add(pin);
            }

            dvm.LoadMachines();
            searchVINComboBox.ItemsSource = dvm.GetMachines;
        }

        public Pushpin AddPushPin(double lat, double lon, string tooltip = "latitude:{0}\nlongitude:{1}")
        {
            Pushpin pin = new Pushpin();
            pin.Location = new Location(lat, lon);
            pin.Content = "";
            pin.ToolTip = string.Format(tooltip, lat, lon);
            return pin;

        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

            var combined = CombinedCalendar.SelectedDate.Value.AddSeconds(CombinedClock.Time.TimeOfDay.TotalSeconds);
            if (!choosingToTime)
            {
                from = combined;
                fromTextBlock.Text = combined.ToString();
            }
            else
            {
                to = combined;
                toTextBlock.Text = combined.ToString();
            }
        }

        private void Sample1_DialogHost_OnDialogOpened(object sender, DialogOpenedEventArgs eventArgs)
        {
            if (choosingToTime)
            {
                if (fromTextBlock.Text.Length > 0)
                {
                    CombinedCalendar.DisplayDateStart = from;
                }
            }
        }

        private void Dialog_Cancel_Click(object sender, RoutedEventArgs e)
        {
            choosingToTime = false;
        }
        private void Cancel_From_Button(object sender, RoutedEventArgs e)
        {
            fromTextBlock.Text = null;
        }

        private void Cancel_To_Button(object sender, RoutedEventArgs e)
        {
            toTextBlock.Text = null;
        }

        private void To_Button_Click(object sender, RoutedEventArgs e)
        {
            choosingToTime = true;
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            if(searchVINComboBox.SelectedValue != null)
            {
                var VIN = searchVINComboBox.SelectedValue.ToString();
                long? startTime = null;
                long? endTime = null;

                if (fromTextBlock.Text.Length > 0)
                {
                    startTime = ((DateTimeOffset)from).ToUnixTimeSeconds();
                }

                if (toTextBlock.Text.Length > 0)
                {
                    endTime = ((DateTimeOffset)to).ToUnixTimeSeconds(); ;
                }

                dvm.SearchData(VIN, null, null, null, startTime, endTime);

                var dataSearch = dvm.GetData;

                myMap.Children.Clear();

                foreach (var d in dataSearch)
                {
                    var pin = AddPushPin(d.GPSlat, d.GPSlon, "VIN: " + d.MachineVIN + "\nlatitude: {0}\nlongitude: {1}");
                    myMap.Children.Add(pin);
                }

                mapTextBlock.Text = "Mapa zobrazuje: Data pro stroj s VIN: " + VIN;
                CancelSearchButton.Visibility = Visibility.Visible;
            }
        }

        private void Cancel_Search_Button_Click(object sender, RoutedEventArgs e)
        {
            fromTextBlock.Text = null;
            toTextBlock.Text = null;

            mapTextBlock.Text = "Mapa zobrazuje: Poslední data";

            myMap.Children.Clear();

            dvm.LoadLastData();
            var lastData = dvm.GetLastData;

            foreach (var d in lastData)
            {
                var pin = AddPushPin(d.GPSlat, d.GPSlon, "VIN: " + d.MachineVIN + "\nlatitude: {0}\nlongitude: {1}");
                myMap.Children.Add(pin);
            }

            CancelSearchButton.Visibility = Visibility.Hidden;
        }
    }
}
