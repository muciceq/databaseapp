﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.ViewModel
{
    class TensiometerViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<TensiometerModel> _tensiometersCollection;
        private ObservableCollection<MachineModel> _machineCollection;

        public TensiometerViewModel()
        {
            _tensiometersCollection = new ObservableCollection<TensiometerModel>();
            _machineCollection = new ObservableCollection<MachineModel>();
        }

        public ObservableCollection<MachineModel> GetMachines
        {
            get { return _machineCollection; }
            set
            {
                _machineCollection = value;
                NotifyPropertyChanged("GetMachines");
            }
        }

        public void GetAllMachines()
        {
            _machineCollection = MachineSQLData.LoadMachines().Result;
        }

        public ObservableCollection<TensiometerModel> GetTensiometers
        {
            get { return _tensiometersCollection; }
            set
            {
                _tensiometersCollection = value;
                NotifyPropertyChanged("GetTensiometers");
            }
        }

        public void GetAllTensiometers()
        {
            _tensiometersCollection = TensiometerSQLData.LoadTensiometers().Result;
        }

        public string InsertTensometer(string vin, bool isActive, string note)
        {
            string output = "Tenzometr přidán do databáze";
            try
            {
                TensiometerSQLData.InsertTensometer(vin, isActive, note);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string DeleteTensometer(Int64 id)
        {
            string output = "Tenzometr odebrán z databáze";
            try
            {
                TensiometerSQLData.DeleteTensometer(id);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string UpdateTensometer(Int64 id, string vin, bool isActive, string note)
        {
            string output = "Tenzometr upraven";
            try
            {
                TensiometerSQLData.UpdateTensometer(id, vin, isActive, note);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public void SearchTensometerByID(Int64 id)
        {
            _tensiometersCollection = TensiometerSQLData.LoadTensiometersByID(id).Result;
        }

        public void SearchTensometerByVIN(string VIN)
        {
            _tensiometersCollection = TensiometerSQLData.LoadTensiometersByVIN(VIN).Result;
        }

        public void SearchTensometerByActiveState(bool state)
        {
            _tensiometersCollection = TensiometerSQLData.LoadTensiometersByActive(state).Result;
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
