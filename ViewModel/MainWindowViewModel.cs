﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using DatabaseApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Text;
using System.Windows.Data;

namespace DatabaseApp.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private int _switchView;

        public MainWindowViewModel()
        {
            /*
            List<MachineModel> getMachines = MachineSQLData.LoadMachines().Result;
            List<MachineModel> getTopNMachines = MachineSQLData.LoadTopNMachines(5).Result;
            MachineModel mach = MachineSQLData.LoadMachineByFullVIN("KR5FI16F76GALK0XF").Result;
            List<MachineModel> getMachinesBySectionOfVIN = MachineSQLData.LoadMachineBySectionOfVIN("kr").Result;
            
            MachineModel testM = new MachineModel("KR5FI16F76GALK0XF", 1, 1);
            string ex = "";
            try
            {
                MachineSQLData.InsertMachine(testM);
            } catch (Exception e)
            {
                ex = e.Message;
            }
            */
        }

        //number of active usercontrol 
        public int SwitchView
        {
            get
            {
                return _switchView;
            }
            set
            {
                _switchView = value;
                NotifyPropertyChanged("SwitchView");
            }
        }

        #region propertychanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
        #endregion
    }
}
