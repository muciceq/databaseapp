﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using DatabaseApp.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.ViewModel
{
    class DataViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<DataModel> _dataCollection;
        private ObservableCollection<DataModel> _lastData;
        private ObservableCollection<MachineModel> _machineCollection;
        // private ObservableCollection<DataModel> _lastData;

        public DataViewModel()
        {
            _dataCollection = new ObservableCollection<DataModel>();
            _lastData = new ObservableCollection<DataModel>();
            _machineCollection = new ObservableCollection<MachineModel>();
        }

        public ObservableCollection<DataModel> GetData
        {
            get { return _dataCollection; }
            set
            {
                _dataCollection = value;
                NotifyPropertyChanged("GetData");
            }
        }

        public void GetAllData()
        {
            // _lastData = DataSQL.LoadLastData().Result;
            _dataCollection = DataSQL.LoadTopNData(100).Result;
        }

        public ObservableCollection<DataModel> GetLastData
        {
            get { return _lastData; }
            set
            {
                _lastData = value;
                NotifyPropertyChanged("GetLastData");
            }
        }

        public void LoadLastData()
        {
            _lastData = DataSQL.LoadLastData().Result;
        }

        public ObservableCollection<MachineModel> GetMachines
        {
            get { return _machineCollection; }
            set
            {
                _machineCollection = value;
                NotifyPropertyChanged("GetMachines");
            }
        }

        public void LoadMachines()
        {
            _machineCollection = MachineSQLData.LoadMachines().Result;
        }

        public void SearchData(string machineVIN = null, int? machineSettingID = null,
            int? unitSerialNumber = null, int? unitSettingID = null, long? startTime = null, long? endTime = null, string state = null,
            int? batteryVoltage = null, int? AxcelerometerX = null, int? AxcelerometerY = null, int? AxcelerometerZ = null,
            int? Tensiometer1 = null, int? Tensiometer2 = null, int? Tensiometer3 = null, int? Tensiometer4 = null, int? Tensiometer5 = null)
        {
            _dataCollection = DataSQL.SearchData(machineVIN, machineSettingID, unitSerialNumber, unitSettingID, startTime,
                endTime, state, batteryVoltage, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1, Tensiometer2,
                Tensiometer3, Tensiometer4, Tensiometer5).Result;
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
