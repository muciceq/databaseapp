﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.ViewModel
{
    class UnitSettingViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<UnitSettingModel> _unitSettingCollection;

        public UnitSettingViewModel()
        {
            _unitSettingCollection = new ObservableCollection<UnitSettingModel>();
        }

        public ObservableCollection<UnitSettingModel> GetUnitSettings
        {
            get { return _unitSettingCollection; }
            set
            {
                _unitSettingCollection = value;
                NotifyPropertyChanged("GetUnitSettings");
            }
        }

        public void GetAllUnitSettings()
        {
            _unitSettingCollection = UnitSettingSQLData.LoadUnitSettings().Result;
        }

        public string InserUnitSetting(int interval, bool batteryState)
        {
            string output = "Nastavení jednotky přidáno do databáze";
            try
            {
                UnitSettingSQLData.InsertUnit(interval, batteryState);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string DeleteUnitSetting(Int64 id)
        {
            string output = "Nastavení jednotky odebráno z databáze";
            try
            {
                UnitSettingSQLData.DeleteUnitSetting(id);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string UpdateUnitSetting(Int64 id, int newInterval, bool newBatteryState)
        {
            string output = "Jednotka upravena";
            try
            {
                UnitSettingSQLData.UpdateUnitSetting(id, newInterval, newBatteryState);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public void SearchUnitSettingByID(Int64 id)
        {
            _unitSettingCollection = UnitSettingSQLData.LoadUnitsSettingBySerialNumber(id).Result;
        }

        public void SearchUnitSettingByInterval(int interval)
        {
            _unitSettingCollection = UnitSettingSQLData.LoadUnitsSettingByInterval(interval).Result;
        }

        public void SearchUnitSettingByState(bool state)
        {
            _unitSettingCollection = UnitSettingSQLData.LoadUnitsSettingByBatteryState(state).Result;
        }


        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
