﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.ViewModel
{
    class UnitViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<UnitSettingModel> _unitSettingCollection;
        private List<int> _unitStatus;
        private ObservableCollection<UnitModelActive> _unitActiveCollection;

        public UnitViewModel()
        {
            _unitSettingCollection = new ObservableCollection<UnitSettingModel>();
            _unitStatus = new List<int>();
        }

        public ObservableCollection<UnitModelActive> GetUnits
        {
            get { return _unitActiveCollection; }
            set
            {
                _unitActiveCollection = value;
                NotifyPropertyChanged("GetUnits");
            }
        }

        public void GetAllUnits()
        {
            _unitActiveCollection = UnitSQLData.LoadUnits().Result;
            _unitStatus = UnitSQLData.LoadUnitsStatus().Result;
            setUnitsStatus();
        }

        public string AddUnit(int unitSettingID)
        {
            string output = "Jednotka přidána do databáze";
            try
            {
                UnitSQLData.InsertUnit(unitSettingID);
            } 
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string DeleteUnit(Int64 id)
        {
            string output = "Jednotka odstraněna z databáze";
            try
            {
                UnitSQLData.DeleteUnit(id);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string UpdateUnit(Int64 id, int newSetting)
        {
            string output = "Jednotka upravena";
            try
            {
                UnitSQLData.UpdateUnit(id, newSetting);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public void SearchUnits(Int64 id)
        {
            _unitActiveCollection = UnitSQLData.LoadUnitsBySerialNumber(id).Result;
            _unitStatus = UnitSQLData.LoadUnitsStatus().Result;
            setUnitsStatus();
        }


        public ObservableCollection<UnitSettingModel> GetUnitSettings
        {
            get { return _unitSettingCollection; }
            set
            {
                _unitSettingCollection = value;
                NotifyPropertyChanged("GetUnitSettings");
            }
        }

        private void setUnitsStatus()
        {
            if (_unitStatus.Count > 0)
            {
                foreach (var u in _unitActiveCollection)
                {
                    if (_unitStatus.IndexOf(Convert.ToInt32(u.UnitSerialNumber)) > -1)
                    {
                        u.State = "Online";
                    }
                }
            }        
        }

        public void GetAllUnitSettings()
        {
            _unitSettingCollection = UnitSettingSQLData.LoadUnitSettings().Result;
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
