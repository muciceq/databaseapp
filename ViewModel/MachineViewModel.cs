﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.ViewModel
{
    class MachineViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<MachineModel> _machineModelsCollection;
        private ObservableCollection<UnitModel> _unitModelCollection;
        private ObservableCollection<MachineSettingModel> _machineSettingCollection;

        public MachineViewModel()
        {
            _machineModelsCollection = new ObservableCollection<MachineModel>();
            _unitModelCollection = new ObservableCollection<UnitModel>();
            _machineSettingCollection = new ObservableCollection<MachineSettingModel>();
        }

        public ObservableCollection<MachineModel> GetMachines
        {
            get
            {
                return _machineModelsCollection;
            }
            set
            {
                _machineModelsCollection = value;
                NotifyPropertyChanged("GetMachines");
            }
        }

        public void SearchMachines(string partialVIN)
        {
            _machineModelsCollection = MachineSQLData.LoadMachineBySectionOfVIN(partialVIN).Result;

        }

        public void GetAllMachines()
        {
            _machineModelsCollection = MachineSQLData.LoadMachines().Result;
        }

        public string AddMachine(string VIN, int UnitSN, int MachineSettingID)
        {
            MachineModel machineToAdd = new MachineModel(VIN, UnitSN, MachineSettingID);
            string output = "Stroj přidán do databáze";
            try
            {
                MachineSQLData.InsertMachine(machineToAdd);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string DeleteMachine(string VIN)
        {
            string output = "Stroj odebrán z databáze";
            try
            {
                MachineSQLData.DeleteMachine(VIN);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string UpdateMachine(string VIN, int UnitSN, int MachineSettingID, string oldVIN) 
        {
            MachineModel machineToUpdate = new MachineModel(VIN, UnitSN, MachineSettingID);
            string output = "Stroj upraven";
            try
            {
                MachineSQLData.UpdateMachine(machineToUpdate, oldVIN);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }


        #region Units
        public ObservableCollection<UnitModel> GetUnits
        {
            get { return _unitModelCollection; }
            set
            {
                _unitModelCollection = value;
                NotifyPropertyChanged("GetUnits");
            }
        }

        public void GetUnusedUnits()
        {
            _unitModelCollection = UnitSQLData.LoadUnusedUnits().Result;
        }
        #endregion


        #region Machine Setting
        public ObservableCollection<MachineSettingModel> GetMachineSettings
        {
            get { return _machineSettingCollection; }
            set
            {
                _machineSettingCollection = value;
                NotifyPropertyChanged("GetMachineSettings");
            }
        }

        public void GetAllMachineSettings()
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettings().Result;
        }

        #endregion


        #region propertychanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
