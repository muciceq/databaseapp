﻿using DatabaseApp.Model;
using DatabaseApp.Model.SQLiteDataAccess;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace DatabaseApp.ViewModel
{
    class MachineSettingViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<MachineSettingModel> _machineSettingCollection;

        public MachineSettingViewModel()
        {
            _machineSettingCollection = new ObservableCollection<MachineSettingModel>();
        }

        public ObservableCollection<MachineSettingModel> GetMachinesSetting
        {
            get { return _machineSettingCollection; }
            set
            {
                _machineSettingCollection = value;
                NotifyPropertyChanged("GetMachinesSetting");
            }
        }

        public void GetAllMachineSettings()
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettings().Result;
        }

        public string AddMachineSetting(bool sendState, bool sendGPS, bool sendAxcel, bool sendTensio)
        {
            string output = "Nastavení stroje přidáno do databáze";
            try
            {
                MachineSettingSQLData.InsertMachineSetting(sendState, sendGPS, sendAxcel, sendTensio);
            } catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string DeleteMachneSetting(Int64 id)
        {
            string output = "Nastavení stroje odebráno";
            try
            {
                MachineSettingSQLData.DeleteMachineSetting(id);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public string UpdateMachineSetting(Int64 id, bool sendState, bool sendGPS, bool sendAxcel, bool sendTensio)
        {
            string output = "Nastavení upraveno";
            try
            {
                MachineSettingSQLData.UpdateMachineSetting(id, sendState, sendGPS, sendAxcel, sendTensio);
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }

        public void SearchByID(Int64 id)
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettingsBySerialNumber(id).Result;
        }

        public void SearchBySendState(bool state)
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettingsBySendState(state).Result;
        }

        public void SearchByGPSState(bool state)
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettingsBySendGPS(state).Result;
        }

        public void SearchByAxcelerometerState(bool state)
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettingsBySendAxcelerometer(state).Result;
        }

        public void SearchByTensometer(bool state)
        {
            _machineSettingCollection = MachineSettingSQLData.LoadMachineSettingsBySendTensiometer(state).Result;
        }

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
