﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    public interface IModel
    {
        public void Insert(string[] args);
        public void Delete(string[] args);
        public void Update(string[] args);
    }
}
