﻿using Dapper;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class MachineSettingSQLData
    {
        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettings()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new DynamicParameters());
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettingsBySerialNumber(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting where MachineSettingID = @ID";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new { ID = id });
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettingsBySendState(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting where SendState = @State";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new { State = state });
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettingsBySendGPS(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting where SendGPS = @State";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new { State = state });
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettingsBySendAxcelerometer(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting where SendAxcelerometer = @State";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new { State = state });
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<MachineSettingModel>> LoadMachineSettingsBySendTensiometer(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select MachineSettingID, SendState, SendGPS, SendAxcelerometer, SendTensiometer from MachineSetting where SendTensiometer = @State";
                var output = await cnn.QueryAsync<MachineSettingModel>(sql, new { State = state });
                return new ObservableCollection<MachineSettingModel>(output);
            }
        }

        public static void InsertMachineSetting(bool sendState, bool sendGPS, bool sendAxcel, bool sendTensio)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "insert into MachineSetting (SendState, SendGPS, SendAxcelerometer, SendTensiometer) values (@SendState, @SendGPS, @SendAxcelerometer, @SendTensiometer)";
                try
                {
                    var output = cnn.Execute(sql, new {
                        SendState = sendState,
                        SendGPS = sendGPS,
                        SendAxcelerometer = sendAxcel,
                        SendTensiometer = sendTensio
                    });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void DeleteMachineSetting(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "delete from MachineSetting where MachineSettingID = @MachineSettingID";
                try
                {
                    var output = cnn.Execute(sql, new { MachineSettingID = id });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void UpdateMachineSetting(Int64 id, bool sendState, bool sendGPS, bool sendAxcel, bool sendTensio)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "UPDATE MachineSetting SET SendState = @SendState, SendGPS = @SendGPS, " +
                    "SendAxcelerometer = @SendAxcelerometer, SendTensiometer = @SendTensiometer  WHERE MachineSettingID = @MachineSettingID";
                try
                {
                    var output = cnn.Execute(sql, new
                    {
                        SendState = sendState,
                        SendGPS = sendGPS,
                        SendAxcelerometer = sendAxcel,
                        SendTensiometer = sendTensio,
                        MachineSettingID = id
                    });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
