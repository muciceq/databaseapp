﻿using Dapper;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class UnitSettingSQLData
    {
        public static async Task<ObservableCollection<UnitSettingModel>> LoadUnitSettings()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSettingID, Interval, SendBatteryState from UnitSetting limit 100";
                var output = await cnn.QueryAsync<UnitSettingModel>(sql, new DynamicParameters());
                return new ObservableCollection<UnitSettingModel>(output);
            }
        }


        public static void InsertUnit(int interval, bool sendBatteryState)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "insert into UnitSetting (Interval, SendBatteryState) values (@Interval, @SendBatteryState)";
                try
                {
                    var output = cnn.Execute(sql, new { Interval = interval, SendBatteryState = sendBatteryState });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void DeleteUnitSetting(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "delete from UnitSetting where UnitSettingID = @UnitSettingID";
                try
                {
                    var output = cnn.Execute(sql, new { UnitSettingID = id });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void UpdateUnitSetting(Int64 id, int interval, bool sendBatteryState)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "UPDATE UnitSetting SET Interval = @Interval, SendBatteryState = @SendBatteryState WHERE UnitSettingID = @ID";
                try
                {
                    var output = cnn.Execute(sql,
                        new
                        {
                            Interval = interval,
                            SendBatteryState = sendBatteryState,
                            ID = id
                        });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static async Task<ObservableCollection<UnitSettingModel>> LoadUnitsSettingBySerialNumber(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSettingID, Interval, SendBatteryState from UnitSetting where UnitSettingID = @ID";
                var output = await cnn.QueryAsync<UnitSettingModel>(sql, new { ID = id });
                return new ObservableCollection<UnitSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<UnitSettingModel>> LoadUnitsSettingByInterval(int interval)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSettingID, Interval, SendBatteryState from UnitSetting where Interval = @Interval";
                var output = await cnn.QueryAsync<UnitSettingModel>(sql, new { Interval = interval });
                return new ObservableCollection<UnitSettingModel>(output);
            }
        }

        public static async Task<ObservableCollection<UnitSettingModel>> LoadUnitsSettingByBatteryState(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSettingID, Interval, SendBatteryState from UnitSetting where SendBatteryState = @State";
                var output = await cnn.QueryAsync<UnitSettingModel>(sql, new { State = state });
                return new ObservableCollection<UnitSettingModel>(output);
            }
        }

        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
