﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class DataSQL
    {
        public static async Task<ObservableCollection<DataModel>> LoadData()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select ID, MachineVIN, MachineSettingID, UnitSerialNumber, UnitSettingID, DateTime, State, GPSlat, " +
                    "GPSlon, Battery, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1, Tensiometer2, Tensiometer3, Tensiometer4, Tensiometer5 From Data";
                var output = await cnn.QueryAsync<DataModel>(sql, new DynamicParameters());
                return new ObservableCollection<DataModel>(output);
            }
        }

        public static async Task<ObservableCollection<DataModel>> LoadTopNData(int count)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select ID, MachineVIN, MachineSettingID, UnitSerialNumber, UnitSettingID, DateTime, State, GPSlat, " +
                    "GPSlon, Battery, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1, Tensiometer2, Tensiometer3, " +
                    "Tensiometer4, Tensiometer5 From Data order by ID desc limit @Count";
                var output = await cnn.QueryAsync<DataModel>(sql, new { Count = count });
                return new ObservableCollection<DataModel>(output);
            }
        }

        public static async Task<ObservableCollection<DataModel>> LoadLastData()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select ID, MachineVIN, MachineSettingID, UnitSerialNumber, UnitSettingID, MAX(DateTime) AS DateTime, State, GPSlat, " +
                    "GPSlon, Battery, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1, Tensiometer2, Tensiometer3, Tensiometer4, Tensiometer5 From Data GROUP BY MachineVIN";
                var output = await cnn.QueryAsync<DataModel>(sql, new DynamicParameters());
                return new ObservableCollection<DataModel>(output);
            }
        }


        public static async Task<ObservableCollection<DataModel>> SearchData(string machineVIN = null, int? machineSettingID = null, 
            int? unitSerialNumber = null, int? unitSettingID = null, long? startTime = null, long? endTime = null, string state = null,
            int? batteryVoltage = null, int? AxcelerometerX = null, int? AxcelerometerY = null, int? AxcelerometerZ = null,
            int? Tensiometer1 = null, int? Tensiometer2 = null, int? Tensiometer3 = null, int? Tensiometer4 = null, int? Tensiometer5 = null)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select ID, MachineVIN, MachineSettingID, UnitSerialNumber, UnitSettingID, DateTime, State, GPSlat, " +
                    "GPSlon, Battery, AxcelerometerX, AxcelerometerY, AxcelerometerZ, Tensiometer1, Tensiometer2, Tensiometer3, " +
                    "Tensiometer4, Tensiometer5 From Data where " +
                    "(@MachineVIN IS NULL OR MachineVIN LIKE @MachineVIN) AND " +
                    "(@MachineSettingID IS NULL OR MachineSettingID = @MachineSettingID) AND " +
                    "(@UnitSerialNumber IS NULL OR UnitSerialNumber = @UnitSerialNumber) AND " +
                    "(@UnitSettingID IS NULL OR UnitSettingID = @UnitSettingID) AND " +
                    "(@StartTime IS NULL OR DateTime >= @StartTime) AND " +
                    "(@EndTime IS NULL OR DateTime <= @EndTime) AND " +
                    "(@State IS NULL OR State LIKE @State) AND " +
                    "(@Battery IS NULL OR Battery = @Battery) AND " +
                    "(@Ax IS NULL OR AxcelerometerX = @Ax) AND " +
                    "(@Ay IS NULL OR AxcelerometerY = @Ay) AND " +
                    "(@Az IS NULL OR AxcelerometerZ = @Az) AND " +
                    "(@T1 IS NULL OR Tensiometer1 = @T1) AND " +
                    "(@T2 IS NULL OR Tensiometer2 = @T2) AND " +
                    "(@T3 IS NULL OR Tensiometer3 = @T3) AND " +
                    "(@T4 IS NULL OR Tensiometer4 = @T4) AND " +
                    "(@T5 IS NULL OR Tensiometer5 = @T5)";
                var output = await cnn.QueryAsync<DataModel>(sql, new { 
                    MachineVIN =  machineVIN,
                    MachineSettingID = machineSettingID,
                    UnitSerialNumber = unitSerialNumber,
                    UnitSettingID = unitSettingID,
                    StartTime = startTime,
                    EndTime = endTime,
                    State = state,
                    Battery = batteryVoltage,
                    Ax = AxcelerometerX,
                    Ay = AxcelerometerY,
                    Az = AxcelerometerZ,
                    T1 = Tensiometer1,
                    T2 = Tensiometer2,
                    T3 = Tensiometer3,
                    T4 = Tensiometer4,
                    T5 = Tensiometer5
                });
                return new ObservableCollection<DataModel>(output);
            }
        }

        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
