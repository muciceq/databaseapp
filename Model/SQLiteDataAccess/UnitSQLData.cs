﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class UnitSQLData
    {
        public static async Task<ObservableCollection<UnitModelActive>> LoadUnits()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSerialNumber, UnitSettingID, 'Offline' AS State from Unit limit 100";
                var output = await cnn.QueryAsync<UnitModelActive>(sql, new DynamicParameters());
                return new ObservableCollection<UnitModelActive>(output);
            }
        }

        public static async Task<ObservableCollection<UnitModelActive>> LoadUnitsBySerialNumber(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSerialNumber, UnitSettingID, 'Offline' AS State from Unit where UnitSerialNumber = @ID";
                var output = await cnn.QueryAsync<UnitModelActive>(sql, new { ID = id });
                return new ObservableCollection<UnitModelActive>(output);
            }
        }

        public static async Task<ObservableCollection<UnitModel>> LoadUnusedUnits()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select UnitSerialNumber, UnitSettingID from Unit WHERE Unit.UnitSerialNumber NOT IN (Select UnitSN From Machine)";
                var output = await cnn.QueryAsync<UnitModel>(sql, new DynamicParameters());
                return new ObservableCollection<UnitModel>(output);
            }
        }

        public static void InsertUnit(int unitSettingID)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "insert into Unit (UnitSettingID) values (@UnitSettingID)";
                try
                {
                    var output = cnn.Execute(sql, new { UnitSettingID = unitSettingID });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void DeleteUnit(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "delete from Unit where UnitSerialNumber = @UnitSerialNumber";
                try
                {
                    var output = cnn.Execute(sql, new { UnitSerialNumber = id });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void UpdateUnit(Int64 id, int unitSettingID)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "UPDATE Unit SET UnitSettingID = @UnitSettingID WHERE UnitSerialNumber = @ID";
                try
                {
                    var output = cnn.Execute(sql,
                        new
                        {
                            UnitSettingID = unitSettingID,
                            ID = id
                        });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static async Task<List<int>> LoadUnitsStatus()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select UnitSerialNumber FROM DATA d WHERE d.DateTime >= strftime('%s', datetime('now', '-5 minutes'))";
                var output = await cnn.QueryAsync<int>(sql, new DynamicParameters());
                return new List<int>(output);
            }
        }


        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
