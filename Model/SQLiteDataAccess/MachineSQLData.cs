﻿using Dapper;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class MachineSQLData
    {
        /// <summary>
        /// Loads all Machines
        /// </summary>
        /// <returns></returns>
        public static async Task<ObservableCollection<MachineModel>> LoadMachines()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select VIN, UnitSN, MachineSettingID from Machine";
                var output = await cnn.QueryAsync<MachineModel>(sql, new DynamicParameters());
                return new ObservableCollection<MachineModel>(output);
            }
        }

        /// <summary>
        /// Loads top N records of Machines
        /// </summary>
        /// <param name="count">number of records</param>
        /// <returns></returns>
        public static async Task<ObservableCollection<MachineModel>> LoadTopNMachines(int count)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select VIN, UnitSN, MachineSettingID from Machine limit @Count";
                var output = await cnn.QueryAsync<MachineModel>(sql, new { Count = count });
                return new ObservableCollection<MachineModel>(output);
            }
        }

        /// <summary>
        /// Search Machine by VIN
        /// Output - only one machine with specific VIN
        /// </summary>
        /// <param name="vin">specific VIN</param>
        /// <returns></returns>
        public static async Task<MachineModel> LoadMachineByFullVIN (string vin)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select VIN, UnitSN, MachineSettingID from Machine where VIN = @VIN";
                var output = await cnn.QuerySingleAsync<MachineModel>(sql, new { VIN = vin });
                return output;
            }
        }

        /// <summary>
        /// Search for machines with VIN like string
        /// </summary>
        /// <param name="partOfVIN">Partial VIN</param>
        /// <returns></returns>
        public static async Task<ObservableCollection<MachineModel>> LoadMachineBySectionOfVIN (string partOfVIN)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "select VIN, UnitSN, MachineSettingID from Machine where VIN LIKE @VIN";
                var output = await cnn.QueryAsync<MachineModel>(sql, new { VIN = "%" + partOfVIN + "%" });
                return new ObservableCollection<MachineModel>(output);
            }
        }

        /// <summary>
        /// Inserts new Machine into database
        /// </summary>
        /// <param name="machine">Machine to insert</param>
        public static void InsertMachine(MachineModel machine)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "insert into Machine (VIN, UnitSN, MachineSettingID) values (@VIN, @UnitSN, @MachineSettingID)";
                try
                {
                    var output = cnn.Execute(sql, machine);
                } catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Delete machine by VIN number
        /// </summary>
        /// <param name="vin">Machine VIN</param>
        public static void DeleteMachine(string vin)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "delete from Machine where VIN = @VIN";
                try
                {
                    var output = cnn.Execute(sql, new { VIN = vin });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Delete machine by VIN number
        /// </summary>
        /// <param name="vin">Machine VIN</param>
        public static void UpdateMachine(MachineModel machine, string odlVin)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "UPDATE Machine SET VIN = @VIN, UnitSN = @UnitSN, MachineSettingID = @MachineSettingID WHERE VIN = @OldVIN";
                try
                {
                    var output = cnn.Execute(sql,
                        new { 
                            VIN = machine.VIN, 
                            UnitSN = machine.UnitSN,
                            MachineSettingID = machine.MachineSettingID,
                            OldVIN = odlVin
                        });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
