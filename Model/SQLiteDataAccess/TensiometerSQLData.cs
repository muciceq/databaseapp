﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApp.Model.SQLiteDataAccess
{
    class TensiometerSQLData
    {
        public static async Task<ObservableCollection<TensiometerModel>> LoadTensiometers()
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select TensiometerID, MachineVIN, IsActive, Note From Tensiometer";
                var output = await cnn.QueryAsync<TensiometerModel>(sql, new DynamicParameters());
                return new ObservableCollection<TensiometerModel>(output);
            }
        }

        public static async Task<ObservableCollection<TensiometerModel>> LoadTensiometersByID(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select TensiometerID, MachineVIN, IsActive, Note From Tensiometer Where TensiometerID = @ID";
                var output = await cnn.QueryAsync<TensiometerModel>(sql, new { ID = id });
                return new ObservableCollection<TensiometerModel>(output);
            }
        }

        public static async Task<ObservableCollection<TensiometerModel>> LoadTensiometersByVIN(string vin)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select TensiometerID, MachineVIN, IsActive, Note From Tensiometer Where MachineVIN LIKE @VIN";
                var output = await cnn.QueryAsync<TensiometerModel>(sql, new { VIN = "%" + vin + "%" });
                return new ObservableCollection<TensiometerModel>(output);
            }
        }

        public static async Task<ObservableCollection<TensiometerModel>> LoadTensiometersByActive(bool state)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "Select TensiometerID, MachineVIN, IsActive, Note From Tensiometer Where IsActive = @State";
                var output = await cnn.QueryAsync<TensiometerModel>(sql, new { State = state });
                return new ObservableCollection<TensiometerModel>(output);
            }
        }

        public static void InsertTensometer(string vin, bool isActive, string note)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "insert into Tensiometer (MachineVIN, IsActive, Note) values (@MachineVIN, @IsActive, @Note)";
                try
                {
                    var output = cnn.Execute(sql, new { MachineVIN = vin, IsActive = isActive, Note = note});
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void DeleteTensometer(Int64 id)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "delete from Tensiometer where TensiometerID = @TensiometerID";
                try
                {
                    var output = cnn.Execute(sql, new { TensiometerID = id });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public static void UpdateTensometer(Int64 id, string vin, bool isActive, string note)
        {
            using (IDbConnection cnn = new SQLiteConnection(GetConnectionString()))
            {
                string sql = "UPDATE Tensiometer SET MachineVIN = @VIN, IsActive = @IsActive, Note = @Note WHERE TensiometerID = @TensiometerID";
                try
                {
                    var output = cnn.Execute(sql,
                        new
                        {
                            VIN = vin,
                            IsActive = isActive,
                            Note = note,
                            TensiometerID = id
                        });
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private static string GetConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
