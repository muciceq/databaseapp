﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    class UnitSettingModel
    {
        public Int64 UnitSettingID { get; set; }
        public int Interval { get; set; }
        public bool SendBatteryState { get; set; }

        public UnitSettingModel(Int64 unitSettingID, int interval, bool sendBatteryState)
        {
            UnitSettingID = unitSettingID;
            Interval = interval;
            SendBatteryState = sendBatteryState;
        }
    }
}
