﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    class MachineSettingModel
    {
        public Int64 MachineSettingID { get; set; }
        public bool SendState { get; set; }
        public bool SendGPS { get; set; }
        public bool SendAxcelerometer { get; set; }
        public bool SendTensiometer { get; set; }

        public MachineSettingModel(Int64 machineSettingID, bool sendState, bool sendGPS, bool sendAxcelerometer, bool sendTensiometer)
        {
            MachineSettingID = machineSettingID;
            SendState = sendState;
            SendGPS = sendGPS;
            SendAxcelerometer = sendAxcelerometer;
            SendTensiometer = sendTensiometer;
        }
    }
}
