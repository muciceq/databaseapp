﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DatabaseApp.Model
{
    class MachineModel : INotifyPropertyChanged
    {
        private string _vin;
        private int _unitSN;
        private int _machineSettingID;

        public string VIN 
        { 
            get { return _vin; }
            set
            {
                _vin = value;
                NotifyPropertyChanged("VIN");
            } 
        }
        public int UnitSN 
        { 
            get { return _unitSN; }
            set
            {
                _unitSN = value;
                NotifyPropertyChanged("UnitSN");
            }
        }
        public int MachineSettingID 
        { 
            get { return _machineSettingID; }
            set
            {
                _machineSettingID = value;
                NotifyPropertyChanged("MachineSettingID");
            } 
        }

        public MachineModel(string vIN, int unitSN, int machineSettingID)
        {
            VIN = vIN;
            UnitSN = unitSN;
            MachineSettingID = machineSettingID;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));

            }
        }
    }
}
