﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    class DataModel
    {
        public Int64 ID { get; set; }
        public string MachineVIN { get; set; }
        public int MachineSettingID { get; set; }
        public int UnitSerialNumber { get; set; }
        public int UnitSettingID { get; set; }
        public DateTime DateTime { get; set; }
        public string State { get; set; }
        public double GPSlat { get; set; }
        public double GPSlon { get; set; }
        public decimal Battery { get; set; }
        public decimal AxcelerometerX { get; set; }
        public decimal AxcelerometerY { get; set; }
        public decimal AxcelerometerZ { get; set; }
        public decimal Tensiometer1 { get; set; }
        public decimal Tensiometer2 { get; set; }
        public decimal Tensiometer3 { get; set; }
        public decimal Tensiometer4 { get; set; }
        public decimal Tensiometer5 { get; set; }


        public DataModel(Int64 iD, string machineVIN, int machineSettingID, int unitSerialNumber, int unitSettingID, long dateTime, string state,
            double gPSlat, double gPSlon, decimal battery, decimal axcelerometerX, decimal axcelerometerY, decimal axcelerometerZ,
            decimal tensiometer1, decimal tensiometer2, decimal tensiometer3, decimal tensiometer4, decimal tensiometer5)
        {
            ID = iD;
            MachineVIN = machineVIN;
            MachineSettingID = machineSettingID;
            UnitSerialNumber = unitSerialNumber;
            UnitSettingID = unitSettingID;
            DateTime = UnixTimeToDateTime(dateTime);
            State = state;
            GPSlat = gPSlat;
            GPSlon = gPSlon;
            Battery = battery;
            AxcelerometerX = axcelerometerX;
            AxcelerometerY = axcelerometerY;
            AxcelerometerZ = axcelerometerZ;
            Tensiometer1 = tensiometer1;
            Tensiometer2 = tensiometer2;
            Tensiometer3 = tensiometer3;
            Tensiometer4 = tensiometer4;
            Tensiometer5 = tensiometer5;
        }

        public DateTime UnixTimeToDateTime(long unixDateTime)
        {
            var localDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixDateTime).ToLocalTime();
            return localDateTime;
        }
    }
}
