﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    class TensiometerModel
    {
        public Int64 TensiometerID { get; set; }
        public string MachineVIN { get; set; }
        public bool IsActive { get; set; }
        public string Note { get; set; }

        public TensiometerModel(Int64 tensiometerID, string machineVIN, bool isActive, string note)
        {
            TensiometerID = tensiometerID;
            MachineVIN = machineVIN;
            IsActive = isActive;
            Note = note;
        }
    }
}
