﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseApp.Model
{
    class UnitModel
    {
        public Int64 UnitSerialNumber { get; set; }
        public int UnitSettingID { get; set; }

        public UnitModel(Int64 unitSerialNumber, int unitSettingID)
        {
            UnitSerialNumber = unitSerialNumber;
            UnitSettingID = unitSettingID;
        }
    }

    class UnitModelActive : UnitModel
    {
        public string State { get; set; }

        public UnitModelActive(Int64 unitSerialNumber, int unitSettingID, string state) : base (unitSerialNumber, unitSettingID)
        {
            State = state;
        }
    }
}
