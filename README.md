# DatabaseApp
## Instalace
Instalace pomocí instalátoru ve složce Installer. Nainstaluje oba programy: databázovou aplikaci (Database App Glos_Snetivy) a generátor (Data Generator Glos_Snetivy).
## Po instalaci
Po instalaci je potřeba provést jeden krok: jít do složky kde jsou aplikace nainstalované, výchozí umístění je: C:\Program Files\GlosSnetivy\DatabaseAppInstaller.
Zde pravé tlačítko na složku Database, zvolit možnost Vlastnosti (Properties), přejít do záložky Zabezpečení (Security), klinout na tlačítko Upravit (Edit), pro skupinu Uživatelé (Users)
zakliknout možnost Upravovat (Modify) viz [obrázek](https://i.stack.imgur.com/Too2c.png).
# Generátor
[Odkaz na zdrojové kódy generátoru](https://gitlab.com/Barthilas/databasemock)

